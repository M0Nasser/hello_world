﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCameraController : MonoBehaviour
{
    public float RotaionSpeed = 1;
    public Transform Target, Player;
    float MouseX, MouseY;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        CameraControll();

    }

     void CameraControll()
    {
       
        MouseX += Input.GetAxis("Mouse X")* RotaionSpeed;
        MouseY -= Input.GetAxis("Mouse Y")* RotaionSpeed;
        MouseY = Mathf.Clamp(MouseY,-35, 60);

        transform.LookAt(Target);

        if ( Input.GetKey(KeyCode.LeftShift))
        {
            Target.rotation = Quaternion.Euler(MouseY, MouseX, 0);
        }
        else
        {
            Target.rotation = Quaternion.Euler(MouseY, MouseX, 0);
            Target.rotation = Quaternion.Euler(0, MouseX, 0);
        }
        
    }
}
