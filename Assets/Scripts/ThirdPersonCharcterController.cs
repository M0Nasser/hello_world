﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharcterController : MonoBehaviour
{
    public float Speed;

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        float horiznotalMovement = Input.GetAxis("Horizontal");
        float VerticalMovement = Input.GetAxis("Vertical");
        Vector3 Movement = new Vector3(horiznotalMovement, 0f, VerticalMovement) * Speed * Time.deltaTime;
        transform.Translate(Movement, Space.Self);

    }
}
